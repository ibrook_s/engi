import Vue from 'vue';
import Application from './components/Application.vue';
import store from './store/Store.js';
import router from './router.js';

Vue.config.errorHandler = function (err, vm, info) {
    alert(err);
    console.log('Find error')
};
Vue.config.warningHandler = function (err, vm, info) {
    alert(err);
    console.log('Find error')
};

new Vue({
    el: '#root',
    components: {
        Application
    },
    store,
    router,
    template: '<Application></Application>'
});

