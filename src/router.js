import Vue from 'vue';
import VueRouter from 'vue-router';

const Auth = () => import('./components/Auth.vue');
const Engi = () => import('./components/Engi.vue');
const Dictionary = () => import('./components/Dictionary.vue');
const Register = () => import('./components/Register.vue');
const NewWord = () => import('./components/NewWord.vue');

Vue.use(VueRouter);
const routes = [
    { path: '/', name: 'engi',component: Engi },
    { path: '/auth', name: 'auth', component: Auth },
    { path: '/dictionary', name: 'dictionary', component: Dictionary },
    { path: '/register', name: 'register', component: Register },
    { path: '/word', name: 'newWord', component: NewWord }
];

// 3. Создаём экземпляр роутера с опцией `routes`
// Можно передать и другие опции, но пока не будем усложнять
export default new VueRouter({
    mode: 'history',
    routes // сокращение от `routes: routes`
});