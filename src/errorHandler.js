export default function (err) {
    alert('Время авторизации истекло, вы будете перенаправлены на страницу авторизации');
    localStorage.removeItem('token');
    window.location = '/auth';
}