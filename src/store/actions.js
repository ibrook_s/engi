import {
        TEST_MUTATION,
        CHANGE_COMPONENT,
        LOAD_ALL_WORDS,
        LOAD_WORDS,
        LOADING,
        LOADED,
        AUTH,
        REGISTER,
        ADD_WORD
        } from './actionsTypes.js';
import { getRandomInt } from '../libs/Math.js';
import errorHandler from '../errorHandler.js';
export default {
    [TEST_MUTATION]({ commit }) {
        setTimeout(() => {
            commit(TEST_MUTATION)
        }, 1000)
    },
    [CHANGE_COMPONENT]({ commit }, payload) {
        commit(CHANGE_COMPONENT, payload)
    },
    [LOAD_ALL_WORDS]({ state, commit }) {
        commit(LOADING);
        const headers = {
            token: state.token
        };
        fetch('http://localhost:3000/api/word', { headers })
            .then((data) => {
                console.log(data.status);
                if (data.status === 200)
                    return data.json();
                throw new Error ('Возникла ошибка, сервер вернул статус ' + data.status)
            })
            .then((words) => {
                commit(LOAD_ALL_WORDS, words);
                commit(LOADED)
            })
            .catch(err => {
                commit(LOADED);
                errorHandler(err);
            });

    },
    [LOAD_WORDS]({ commit, state }, data) {
        commit(LOADING);
        const headers = {
            token: state.token
        };
        fetch('http://localhost:3000/api/word/' + data.amount + '/' + data.category, { headers })
            .then((data) => {
                if (data.status !== 403)
                    return data.json();
                throw new Error ('Возникла ошибка, сервер вернул статус ' + data.status)
            })
            .then((words) => {
                if (words) {
                    console.log(words);
                    commit(LOAD_WORDS, words);
                    commit(LOADED)
                }
            })
            .catch(err => {
                commit(LOADED);
                errorHandler(err);
            });
    },
    [AUTH]({ commit, state }, payload) {
        commit(LOADING);
        const params = {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(payload)
        };
        console.log('АВТОРИЗАЦИЯ');
        fetch('http://localhost:3000/api/login', params)
            .then(data => data.json())
            .then(data => {
                if (data.token) {
                    console.log(data);
                    commit(AUTH, data.token);
                } else {
                    alert(data.statusMessage);
                }
                commit(LOADED);
            })
            .catch((err) => {
                console.log('При загрузке произошла ошибка', err);
                alert('Повторите позже')
            })
    },
    [REGISTER]({ commit, state}, payload) {
        commit(LOADING);
        const params = {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(payload)
        };
        fetch('http://localhost:3000/api/register', params)
            .then(data => data.json())
            .then(data => {
                console.log(data);
                alert(data.statusMessage);
                commit(LOADED);
            })
    },
    [ADD_WORD]({ commit, state }, payload) {
        commit(LOADING);
        console.log('Добавление слова', payload);
        const params = {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(payload),
            headers: {
                token: state.token
            }
        };
        fetch('http://localhost:3000/api/word', params)
            .then(data => data.json())
            .then(json => {
                commit(LOADED);
                console.log(json);
            })
            .catch(err => {
                commit(LOADED);
                alert('ERROR', err);
            })
    }
};