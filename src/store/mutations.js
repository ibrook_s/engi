import { TEST_MUTATION, INIT, CHANGE_COMPONENT, LOAD_ALL_WORDS, LOAD_WORDS, LOADING, LOADED, AUTH, REGISTER, RESET_WORDS } from './actionsTypes.js';

export default {
    [TEST_MUTATION](state) {
        state.counter++;
    },
    [INIT](state) {
        const token = localStorage.getItem('token');
        console.log('olololo', token);
        if (token !== null) state.token = token;
    },
    [CHANGE_COMPONENT](state, payload) {
        state.currentComponent = payload
    },
    [LOAD_ALL_WORDS](state, payload) {
        state.allWords = payload;
    },
    [LOAD_WORDS](state, payload) {
        state.words = state.words.concat(...payload);
    },
    [LOADING](state) {
        state.loading = true;
    },
    [LOADED](state) {
        state.loading = false;
    },
    [AUTH](state, payload) {
        state.token = payload;
        localStorage.setItem('token', payload);
    },
    [RESET_WORDS](state) {
        state.words = [];
    }
}