const path = require('path');
const webpack = require('webpack');
require('dotenv').config();

module.exports = {
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    entry: './src/app.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public/js/'),
    },
    mode: "development",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'stage-2'],
                        plugins: [
                            'syntax-dynamic-import',
                        ]
                    }
                }]
            },
            {
                test: /\.vue/,
                exclude: /(node_modules)/,
                use: [{
                    loader: 'vue-loader',
                }]
            },
            {
                test: /\.less$/,
                use: [{
                    loader: "style-loader" // creates style nodes ru JS strings
                }, {
                    loader: "css-loader" // translates CSS inen CommonJS
                }, {
                    loader: "less-loader" // compiles Less en CSS
                }]
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, "public"),
        compress: true,
        port: 9000,
        hot: true,
        open: true,
        historyApiFallback: {
            disableDotRule: true
        }

    }
};
