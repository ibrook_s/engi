const jwtPlugin = require('../plugins/jwt.js');

module.exports = function (ctx, next) {
    console.log(ctx.request.headers.token);
    const token = ctx.request.headers.token;
    console.log(ctx.request.headers.token);
    if (!jwtPlugin.check(token)) {
        ctx.status = 403;
        return;
    }
    return next();
};