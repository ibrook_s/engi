const mongoose = require('mongoose');
const Word = mongoose.model('Word', {
    ru: {
        type: [String],
        required: true,
        minlength: 1,
        maxlength: 30
    },
    en: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 30
    },
    category: {
        type: String,
        default: 'Общение',
        required: true
    },
    isModerate: {
        type: Boolean,
        required: true,
        default: false,
    }
});

module.exports = Word;