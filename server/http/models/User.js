const mongoose = require('mongoose');
const User = mongoose.model('User', {
    name: {
        type: String,
    },
    login: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    email: String,
    token: String,
    cityId: Number
});

module.exports = User;