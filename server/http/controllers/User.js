const User = require('../models/User.js');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const jwtPlugin = require('../plugins/jwt.js');
module.exports = {
    async whoami(ctx, next) {
        const token = ctx.request.headers.token;
        const user = await User.findOne({
            login: jwt.decode(token).login
        });
        ctx.body = JSON.stringify({
            name: user.name,
            login: user.login,
            phone: user.phone,
        });
    },
    async login(ctx) {
        const params = JSON.parse(ctx.request.body);
        let user = await User.findOne({
            login: params.login
        });
        console.log(params, user);

        if (!user) {
            ctx.body = JSON.stringify({
                statusCode: 0,
                statusMessage: 'Пользователь не существует.'
            });
            return;
        }

        if (user.password === params.password) {
            const token = jwt.sign({
                    name: params.name,
                    login: params.login
                },
                'secret',
                {
                    expiresIn: '1h'
                });
            ctx.body = JSON.stringify({
                token,
            });
        } else {
            ctx.body = JSON.stringify({
                statusCode: 0,
                statusMessage: 'Имя пользователя или пароль введены неверно.'
            })
        }
    },
    async register(ctx) {
        const params = JSON.parse(ctx.request.body);
        console.log(params);
        const user = new User({
            name: params.name,
            login: params.login,
            password: params.password,
            email: params.mail,
        });

        try {
            await user.save();
            ctx.status = 201;
            ctx.body = JSON.stringify({
                statusCode: 1,
                statusMessage: 'Пользователь зарегистрирован.'
            });
        } catch (err) {
            console.log(err);
            ctx.body = JSON.stringify({
                statusCode: 0,
                statusMessage: 'Пользователь с таким логином уже существует.'
            });
        }
    }
};