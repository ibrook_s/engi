const Word = require('../models/Word.js');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const jwtPlugin = require('../plugins/jwt.js');
module.exports = {
    async getAll(ctx) {
        const words = await Word.find({isModerate: true});
        ctx.body = JSON.stringify(words);
    },
    async getAmount(ctx) {
        const words = await Word.aggregate(
            [{
                $match: {isModerate: true, category: ctx.params.category}
            },
            {
                $sample: { size: +(ctx.params.amount) }
            },]
        );
        ctx.body = JSON.stringify(words);
    },
    async add(ctx) {
        const data = JSON.parse(ctx.request.body);
        console.log('Мне пришло - ', data);
        let word = new Word({
            en: data.en,
            ru: data.ru,
            category: data.category
        });

        try {
            await word.save();
            ctx.body = JSON.stringify({
                statusCode: 'success',
                statusMessage: 'Слово успешно добавлено'
            })
        } catch (err) {
            ctx.body = JSON.stringify({
                statusCode: 'error',
                statusMessage: 'Ошибка при добавлении слова', err,
            })
        }
    }
};