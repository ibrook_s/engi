const jwt = require('jsonwebtoken');
module.exports = {
    check(token) {
        const decoded = jwt.decode(token);
        if (decoded) {
            if (decoded.exp >= Math.floor(Date.now() / 1000)) {
                console.log(decoded.exp , Math.floor(Date.now() / 1000));
                return true
            }
        }
        return false;
    }
};