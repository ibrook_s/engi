const Router = require('koa-trie-router');
const router = new Router();

const jwtMiddleware = require('../middlewares/jwt.js');

const User = require('../controllers/User.js');
const Word = require('../controllers/Word.js');
router
    .get('/api/', (ctx) => {
        ctx.body = JSON.stringify({
            message: 'API v0.01 сервиса Online Shops'
        });
    })
    .post('/api/register', User.register)
    .post('/api/login', User.login)
    .post('/api/test', User.test)
    .get('/api/me', [jwtMiddleware, User.whoami])

    .get('/api/word', [jwtMiddleware, Word.getAll])
    .get('/api/word/:amount/:category', [jwtMiddleware, Word.getAmount])

    .post('/api/word', [jwtMiddleware, Word.add]);

module.exports = router;